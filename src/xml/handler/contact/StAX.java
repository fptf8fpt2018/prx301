/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package xml.handler.contact;

//import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
//dependency: https://www.findjar.com/jar/com/sun/xml/txw2/txw2/20110809/txw2-20110809.jar.html
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;
import generated.Root;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Admin
 */
public class StAX {

    public List<Root.Contact> contacts;
    Boolean flagFirstName, flagLastName, flagCompanyName, flagAddress, flagCity,
            flagCounty, flagState, flagZip, flagPhone1, flagPhone, flagEmail;

    public StAX() {
        contacts = new ArrayList<>();
        flagFirstName = flagLastName = flagCompanyName = flagAddress = flagCity
                = flagCounty = flagState = flagZip = flagPhone1 = flagPhone
                = flagEmail = false;

    }

    //read data from file then save in field contacts as List<Root.Contact>
    public void readFile(String filePath) {
        try {
            XMLInputFactory factory = XMLInputFactory.newFactory();
            InputStream in = new FileInputStream(filePath);
            XMLEventReader reader = factory.createXMLEventReader(in); //like cursor

            Root.Contact contact = new Root.Contact();//temporary

            //khi nao con doc duoc
            while (reader.hasNext()) {
                XMLEvent evt = reader.nextEvent();
                //xem su kien do la su kien nao
                switch (evt.getEventType()) {
                    case XMLEvent.START_ELEMENT: {
                        StartElement startEle = evt.asStartElement();
                        String startEleName = startEle.getName().getLocalPart();//skip namespace prefix
                        //Root.Contact co attribute nen se xu ly rieng
                        if (startEleName.equalsIgnoreCase("contact")) {
                            Iterator<Attribute> attrs = startEle.getAttributes();
                            String id = attrs.next().getValue();//id
                            contact.setId(id);
                        } //flag cho phan event character
                        else {
                            if (startEleName.equalsIgnoreCase("first_name")) {
                                flagFirstName = true;
                            } else if (startEleName.equalsIgnoreCase("last_name")) {
                                flagLastName = true;
                            } else if (startEleName.equalsIgnoreCase("company_name")) {
                                flagCompanyName = true;
                            } else if (startEleName.equalsIgnoreCase("address")) {
                                flagAddress = true;
                            } else if (startEleName.equalsIgnoreCase("city")) {
                                flagCity = true;
                            } else if (startEleName.equalsIgnoreCase("county")) {
                                flagCounty = true;
                            } else if (startEleName.equalsIgnoreCase("state")) {
                                flagState = true;
                            } else if (startEleName.equalsIgnoreCase("zip")) {
                                flagZip = true;
                            } else if (startEleName.equalsIgnoreCase("phone1")) {
                                flagPhone1 = true;
                            } else if (startEleName.equalsIgnoreCase("phone")) {
                                flagPhone = true;
                            } else if (startEleName.equalsIgnoreCase("email")) {
                                flagEmail = true;
                            }
                        }
                        break;
                    }
                    case XMLEvent.END_ELEMENT: {
                        EndElement endEle = evt.asEndElement();
                        String endEleName = endEle.getName().getLocalPart();

                        //if close tag book, read completed a record in XML.
                        if (endEleName.equalsIgnoreCase("contact")) {
                            contacts.add(contact);
                            contact = new Root.Contact(); //reset for recording next record
                        }//close cac tag khac, reset co
                        else {
                            if (endEleName.equalsIgnoreCase("first_name")) {
                                flagFirstName = false;
                            } else if (endEleName.equalsIgnoreCase("last_name")) {
                                flagLastName = false;
                            } else if (endEleName.equalsIgnoreCase("company_name")) {
                                flagCompanyName = false;
                            } else if (endEleName.equalsIgnoreCase("address")) {
                                flagAddress = false;
                            } else if (endEleName.equalsIgnoreCase("city")) {
                                flagCity = false;
                            } else if (endEleName.equalsIgnoreCase("county")) {
                                flagCounty = false;
                            } else if (endEleName.equalsIgnoreCase("state")) {
                                flagState = false;
                            } else if (endEleName.equalsIgnoreCase("zip")) {
                                flagZip = false;
                            } else if (endEleName.equalsIgnoreCase("phone1")) {
                                flagPhone1 = false;
                            } else if (endEleName.equalsIgnoreCase("phone")) {
                                flagPhone = false;
                            } else if (endEleName.equalsIgnoreCase("email")) {
                                flagEmail = false;
                            }
                        }
                        break;
                    }
                    case XMLEvent.CHARACTERS: {
                        Characters characters = evt.asCharacters();
                        String data = characters.getData();
                        //data set cho truong du lieu nao,
                        //theo the name duoc dat co o su kien open element.
                        //sau do cho flag bang false vi da duyet qua element content do.
                        if (flagFirstName) {
                            contact.setFirstName(data);
                        } else if (flagLastName) {
                            contact.setLastName(data);
                        } else if (flagCompanyName) {
                            contact.setCompanyName(data);
                        } else if (flagAddress) {
                            contact.setAddress(data);
                        } else if (flagCity) {
                            contact.setCity(data);
                        } else if (flagCounty) {
                            contact.setCounty(data);
                        } else if (flagState) {
                            contact.setState(data);
                        } else if (flagZip) {
                            contact.setZip(new BigInteger(data));
                        } else if (flagPhone1) {
                            contact.setPhone1(data);
                        } else if (flagPhone) {
                            contact.setPhone(data);
                        } else if (flagEmail) {
                            contact.setEmail(data);
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        } //Handle when file exception occurs.
        catch (FileNotFoundException | NumberFormatException | XMLStreamException e) {
        }
    }

    //write to filePath a source data.
    public Boolean writeFile(String filePath, List<Root.Contact> source) {
        try {
            XMLOutputFactory factory = XMLOutputFactory.newFactory();
            OutputStream out = new FileOutputStream(filePath);
            XMLStreamWriter streamWriter = factory.createXMLStreamWriter(out);
            IndentingXMLStreamWriter writer
                    = new IndentingXMLStreamWriter(streamWriter);

            writer.writeStartDocument();
            //<root>
            writer.writeStartElement("root");

            //Traverse same structure XML tag as obj
            for (Root.Contact contact : source) {
                //<contact attr1 = "">content
                writer.writeStartElement("contact");
                writer.writeAttribute("id", contact.getId());

                //<first_name>content</first_name>
                writer.writeStartElement("first_name");
                writer.writeCharacters(contact.getFirstName());
                writer.writeEndElement();

                //<last_name>content</last_name>
                writer.writeStartElement("last_name");
                writer.writeCharacters(contact.getLastName());
                writer.writeEndElement();

                //<company_name>content</company_name>
                writer.writeStartElement("company_name");
                writer.writeCharacters(contact.getCompanyName());
                writer.writeEndElement();

                //<address>content</address>
                writer.writeStartElement("address");
                writer.writeCharacters(contact.getAddress());
                writer.writeEndElement();

                //<city>content</city>
                writer.writeStartElement("city");
                writer.writeCharacters(contact.getCity());
                writer.writeEndElement();

                //<county>content</county>
                writer.writeStartElement("county");
                writer.writeCharacters(contact.getCounty());
                writer.writeEndElement();

                //<state>content</state>
                writer.writeStartElement("state");
                writer.writeCharacters(contact.getState());
                writer.writeEndElement();

                //<zip>content</zip>
                writer.writeStartElement("zip");
                writer.writeCharacters(contact.getZip().toString());
                writer.writeEndElement();

                //<phone1>content</phone1>
                writer.writeStartElement("phone1");
                writer.writeCharacters(contact.getPhone1());
                writer.writeEndElement();

                //<phone>content</phone>
                writer.writeStartElement("phone");
                writer.writeCharacters(contact.getPhone());
                writer.writeEndElement();

                //<email>content</email>
                writer.writeStartElement("email");
                writer.writeCharacters(contact.getEmail());
                writer.writeEndElement();

                //</contact>
                writer.writeEndElement();
            }

            //</root>
            writer.writeEndElement();

            writer.writeEndDocument();

            //thus su moi ghi vao file, ben tren chi ghi vao cache
            writer.flush();
            writer.close();//dong luong

            return true;
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return false;
    }
}
