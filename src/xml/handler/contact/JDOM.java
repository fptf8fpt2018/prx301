/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package xml.handler.contact;

import generated.Root;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Admin
 */
public class JDOM {

    public List<Root.Contact> contacts;

    public JDOM() {
        contacts = new ArrayList<>();
    }

    //read data from file then save in field contacts as List<Root.Contact>
    public void readFile(String filePath) {
        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(new File(filePath));
            //System.out.println("Root element :" + document.getRootElement().getName());
            Element eleRoot = document.getRootElement();

            List<Element> elementsContact = eleRoot.getChildren();
            for (Element eleContact : elementsContact) {
                Root.Contact contact = new Root.Contact();
                contact.setId(eleContact.getAttributeValue("id"));
                contact.setFirstName(eleContact.getChild("first_name").getText());
                contact.setLastName(eleContact.getChild("last_name").getText());
                contact.setCompanyName(eleContact.getChild("company_name").getText());
                contact.setAddress(eleContact.getChild("address").getText());
                contact.setCity(eleContact.getChild("city").getText());
                contact.setCounty(eleContact.getChild("county").getText());
                contact.setState(eleContact.getChild("state").getText());
                contact.setZip(new BigInteger(eleContact.getChild("zip").getText()));
                contact.setPhone1(eleContact.getChild("phone1").getText());
                contact.setPhone(eleContact.getChild("phone").getText());
                contact.setEmail(eleContact.getChild("email").getText());

                contacts.add(contact);
            }
        } catch (NumberFormatException e) {
            // TODO: handle exception
        } catch (JDOMException | IOException ex) {
            Logger.getLogger(JDOM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //write to filePath a source data.
    public Boolean writeFile(String filePath, List<Root.Contact> source) {
        try {
            //root element
            Element root = new Element("root");
            Document document = new Document(root);

            for (Root.Contact contact : source) {
                Element eleContact = new Element("contact");
                eleContact.setAttribute(new Attribute("id", contact.getId()));

                Element eleFirstName = new Element("first_name");
                eleFirstName.setText(contact.getFirstName());
                eleContact.addContent(eleFirstName);

                Element eleLastName = new Element("last_name");
                eleLastName.setText(contact.getLastName());
                eleContact.addContent(eleLastName);

                Element eleCompanyName = new Element("company_name");
                eleCompanyName.setText(contact.getCompanyName());
                eleContact.addContent(eleCompanyName);

                Element eleAddress = new Element("address");
                eleAddress.setText(contact.getAddress());
                eleContact.addContent(eleAddress);

                Element eleCity = new Element("city");
                eleCity.setText(contact.getCity());
                eleContact.addContent(eleCity);

                Element eleCounty = new Element("county");
                eleCounty.setText(contact.getCounty());
                eleContact.addContent(eleCounty);

                Element eleState = new Element("state");
                eleState.setText(contact.getState());
                eleContact.addContent(eleState);

                Element eleZip = new Element("zip");
                eleZip.setText(contact.getZip().toString());
                eleContact.addContent(eleZip);

                Element elePhone1 = new Element("phone1");
                elePhone1.setText(contact.getPhone1());
                eleContact.addContent(elePhone1);

                Element elePhone = new Element("phone");
                elePhone.setText(contact.getPhone());
                eleContact.addContent(elePhone);

                Element eleEmail = new Element("email");
                eleEmail.setText(contact.getEmail());
                eleContact.addContent(eleEmail);

                Element eleRoot = document.getRootElement();
                eleRoot.addContent(eleContact);
            }

            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(document, new FileWriter(filePath));

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
