/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package xml.handler.contact;

import generated.Root;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import xml.parser.DOMParser;

/**
 *
 * @author Admin
 */
public class DOM {

    public final List<Root.Contact> contacts;

    public DOM() {
        this.contacts = new ArrayList<>();
    }

    //Read file xml then return a collection
    //XML --> Document(Node, Element) --> Collection
    public List<Root.Contact> readFile(String filePath) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            //SAX truyen handler(extends DefaultHandler) de tham tri, DOM return
            Document document = builder.parse(new File(filePath));
            //chuan hoa bo het ky tu thua(xuong dong, space)
            document.getDocumentElement().normalize();

            NodeList nodesContact = document.getElementsByTagName("contact");
            //Traverse each node of contact one by one.
            for (int i = 0; i < nodesContact.getLength(); i++) {
                Node nodeContact = nodesContact.item(i);
                Element eleContact = (Element) nodeContact;

                Root.Contact contact = new Root.Contact();
                //Get contact attribute is id
                String contactId = eleContact.getAttribute("id");
                contact.setId(contactId);

                //Get contact child element is first_name
                Node nodeFirstName = eleContact.getElementsByTagName("first_name").item(0);
                Element eleFirstName = (Element) nodeFirstName;
                String contactFirstName = eleFirstName.getTextContent();
                contact.setFirstName(contactFirstName);

                //Get contact child element is last_name
                Node nodeLastName = eleContact.getElementsByTagName("last_name").item(0);
                Element eleLastName = (Element) nodeLastName;
                String contactLastName = eleLastName.getTextContent();
                contact.setLastName(contactLastName);

                //Get contact child element is company_name
                Node nodeCompanyName = eleContact.getElementsByTagName("company_name").item(0);
                Element eleCompanyName = (Element) nodeCompanyName;
                String contactCompanyName = eleCompanyName.getTextContent();
                contact.setCompanyName(contactCompanyName);

                //Get contact child element is address
                Node nodeAddress = eleContact.getElementsByTagName("address").item(0);
                Element eleAddress = (Element) nodeAddress;
                String contactAddress = eleAddress.getTextContent();
                contact.setAddress(contactAddress);

                //Get contact child element is city
                Node nodeCity = eleContact.getElementsByTagName("city").item(0);
                Element eleCity = (Element) nodeCity;
                String contactCity = eleCity.getTextContent();
                contact.setCity(contactCity);

                //Get contact child element is county
                Node nodeCounty = eleContact.getElementsByTagName("county").item(0);
                Element eleCounty = (Element) nodeCounty;
                String contactCounty = eleCounty.getTextContent();
                contact.setCounty(contactCounty);

                //Get contact child element is state
                Node nodeState = eleContact.getElementsByTagName("state").item(0);
                Element eleState = (Element) nodeState;
                String contactState = eleState.getTextContent();
                contact.setState(contactState);

                //Get contact child element is zip
                Node nodeZip = eleContact.getElementsByTagName("zip").item(0);
                Element eleZip = (Element) nodeZip;
                String contactZip = eleZip.getTextContent();
                contact.setZip(new BigInteger(contactZip));

                //Get contact child element is phone1
                Node nodePhone1 = eleContact.getElementsByTagName("phone1").item(0);
                Element elePhone1 = (Element) nodePhone1;
                String contactPhone1 = elePhone1.getTextContent();
                contact.setPhone1(contactPhone1);

                //Get contact child element is phone
                Node nodePhone = eleContact.getElementsByTagName("phone").item(0);
                Element elePhone = (Element) nodePhone;
                String contactPhone = elePhone.getTextContent();
                contact.setPhone(contactPhone);

                //Get contact child element is email
                Node nodeEmail = eleContact.getElementsByTagName("email").item(0);
                Element eleEmail = (Element) nodeEmail;
                String contactEmail = eleEmail.getTextContent();
                contact.setEmail(contactEmail);

                contacts.add(contact);
            }

            return contacts;

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DOM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //write [source] to well-formed XML file locate at [filePath]
    //collection --> document --> XML(tranformer, source, stream result)
    public Boolean writeFile(String filePath, List<Root.Contact> source) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.newDocument();

            Element eleRoot = document.createElement("root");
            document.appendChild(eleRoot);

            //traverse contact element one by one
            for (int i = 0; i < source.size(); i++) {
                Element eleContact = document.createElement("contact");
                eleRoot.appendChild(eleContact);

                Root.Contact curContact = source.get(i);
                // add attribute id for element contact
                Attr attrContact = document.createAttribute("id");
                attrContact.setValue(curContact.getId());
                eleContact.setAttributeNode(attrContact);

                // add child element first_name for element contact
                Element eleFirstName = document.createElement("first_name");
                eleFirstName.setTextContent(curContact.getFirstName());
                eleContact.appendChild(eleFirstName);

                // add child element last_name for element contact
                Element eleLastName = document.createElement("last_name");
                eleLastName.setTextContent(curContact.getLastName());
                eleContact.appendChild(eleLastName);

                // add child element company_name for element contact
                Element eleCompanyName = document.createElement("company_name");
                eleCompanyName.setTextContent(curContact.getCompanyName());
                eleContact.appendChild(eleCompanyName);

                // add child element address for element contact
                Element eleAddress = document.createElement("address");
                eleAddress.setTextContent(curContact.getAddress());
                eleContact.appendChild(eleAddress);

                // add child element city for element contact
                Element eleCity = document.createElement("city");
                eleCity.setTextContent(curContact.getCity());
                eleContact.appendChild(eleCity);

                // add child element county for element contact
                Element eleCountry = document.createElement("county");
                eleCountry.setTextContent(curContact.getCounty());
                eleContact.appendChild(eleCountry);

                // add child element state for element contact
                Element eleState = document.createElement("state");
                eleState.setTextContent(curContact.getState());
                eleContact.appendChild(eleState);

                // add child element zip for element contact
                Element eleZip = document.createElement("zip");
                eleZip.setTextContent(curContact.getZip().toString());
                eleContact.appendChild(eleZip);

                // add child element phone1 for element contact
                Element elePhone1 = document.createElement("phone1");
                elePhone1.setTextContent(curContact.getPhone1());
                eleContact.appendChild(elePhone1);

                // add child element phone for element contact
                Element elePhone = document.createElement("phone");
                elePhone.setTextContent(curContact.getPhone());
                eleContact.appendChild(elePhone);

                // add child element email for element contact
                Element eleEmail = document.createElement("email");
                eleEmail.setTextContent(curContact.getEmail());
                eleContact.appendChild(eleEmail);
            }

            TransformerFactory transformerFactory
                    = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            //format khong con tren 1 dong
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //How to format the xml in java dom parser?
            //https://stackoverflow.com/questions/18607343/how-to-format-the-xml-in-java-dom-parser
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            //document tro thanh cai source
            DOMSource DOMSourceObj = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(filePath));
            transformer.transform(DOMSourceObj, streamResult);
            return true;
        } catch (ParserConfigurationException | TransformerConfigurationException ex) {
            Logger.getLogger(DOMParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(DOMParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    //append list to existed xml document
    //ref: https://stackoverflow.com/questions/6445828/how-do-i-append-a-node-to-an-existing-xml-file-in-java
    public Boolean appendFile(String filePath, List<Root.Contact> appendContacts) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            //SAX truyen handler(extends DefaultHandler) de tham tri, DOM return
            Document document = builder.parse(new File(filePath));

            document.getDocumentElement().normalize();

            Element eleRoot = document.getDocumentElement();

            //Tranverse one by one append to existed file
            appendContacts.stream().map(appendContact -> {
                Element eleContact = document.createElement("contact");
                //Set attribute id for contact tag
                Attr attrId = document.createAttribute("id");
                attrId.setValue(appendContact.getId());
                eleContact.setAttributeNode(attrId);
                //Set child tag first_name for contact tag
                Element eleFirstName = document.createElement("first_name");
                eleFirstName.setTextContent(appendContact.getFirstName());
                eleContact.appendChild(eleFirstName);
                //Set child tag last_name for contact tag
                Element eleLastName = document.createElement("last_name");
                eleLastName.setTextContent(appendContact.getLastName());
                eleContact.appendChild(eleLastName);
                //Set child tag company_name for contact tag
                Element eleCompanyName = document.createElement("company_name");
                eleCompanyName.setTextContent(appendContact.getCompanyName());
                eleContact.appendChild(eleCompanyName);
                //Set child tag address for contact tag
                Element eleAddress = document.createElement("address");
                eleAddress.setTextContent(appendContact.getAddress());
                eleContact.appendChild(eleAddress);
                //Set child tag city for contact tag
                Element eleCity = document.createElement("city");
                eleCity.setTextContent(appendContact.getCity());
                eleContact.appendChild(eleCity);
                //Set child tag county for contact tag
                Element eleCounty = document.createElement("county");
                eleCounty.setTextContent(appendContact.getCounty());
                eleContact.appendChild(eleCounty);
                //Set child tag state for contact tag
                Element eleState = document.createElement("state");
                eleState.setTextContent(appendContact.getState());
                eleContact.appendChild(eleState);
                //Set child tag zip for contact tag
                Element eleZip = document.createElement("zip");
                eleZip.setTextContent(appendContact.getZip().toString());
                eleContact.appendChild(eleZip);
                //Set child tag phone1 for contact tag
                Element elePhone1 = document.createElement("phone1");
                elePhone1.setTextContent(appendContact.getPhone1());
                eleContact.appendChild(elePhone1);
                //Set child tag phone for contact tag
                Element elePhone = document.createElement("phone");
                elePhone.setTextContent(appendContact.getPhone());
                eleContact.appendChild(elePhone);
                //Set child tag email for contact tag
                Element eleEmail = document.createElement("email");
                eleEmail.setTextContent(appendContact.getEmail());
                eleContact.appendChild(eleEmail);
                return eleContact;
            }).forEachOrdered(eleContact -> {
                eleRoot.appendChild(eleContact);
            });

            TransformerFactory transformerFactory
                    = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            //format khong con tren 1 dong
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //How to format the xml in java dom parser?
            //https://stackoverflow.com/questions/18607343/how-to-format-the-xml-in-java-dom-parser
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            //document tro thanh cai source
            DOMSource DOMSourceObj = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(filePath));
            transformer.transform(DOMSourceObj, streamResult);
            return true;
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException ex) {
            Logger.getLogger(DOMParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
