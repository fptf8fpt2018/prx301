/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package xml.handler.contact;

import generated.Root;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Admin
 */
public class SAX extends DefaultHandler {

    public final List<Root.Contact> contacts;
    int size;
    boolean flagFirstName, flagLastName, flagCompanyName, flagAddress, flagCity,
            flagCounty, flagState, flagZip, flagPhone1, flagPhone, flagEmail,
            flagId;

    public SAX() {
        contacts = new ArrayList<>();
        size = 0;
        resetDataTag();//Set default value.
    }

    public final void resetDataTag() {
        flagFirstName = false;
        flagLastName = false;
        flagCompanyName = false;
        flagAddress = false;
        flagCity = false;
        flagCounty = false;
        flagState = false;
        flagZip = false;
        flagPhone1 = false;
        flagPhone = false;
        flagEmail = false;
        flagId = false;
    }

    //doc tung dung tu vi tri, lay chieu dai cua dong do
    @Override
    public void characters(char[] chars, int start, int length) throws SAXException {
        String data = new String(chars, start, length);

        //Check field of data match with current obj'field.
        if (flagFirstName) {
            Root.Contact contact = contacts.get(size);
            contact.setFirstName(data);
        } else if (flagLastName) {
            Root.Contact contact = contacts.get(size);
            contact.setLastName(data);
        } else if (flagCompanyName) {
            Root.Contact contact = contacts.get(size);
            contact.setCompanyName(data);
        } else if (flagAddress) {
            Root.Contact contact = contacts.get(size);
            contact.setAddress(data);
        } else if (flagCity) {
            Root.Contact contact = contacts.get(size);
            contact.setCity(data);
        } else if (flagCounty) {
            Root.Contact contact = contacts.get(size);
            contact.setCounty(data);
        } else if (flagState) {
            Root.Contact contact = contacts.get(size);
            contact.setState(data);
        } else if (flagZip) {
            Root.Contact contact = contacts.get(size);
            contact.setZip(new BigInteger(data));
        } else if (flagPhone1) {
            Root.Contact contact = contacts.get(size);
            contact.setPhone1(data);
        } else if (flagPhone) {
            Root.Contact contact = contacts.get(size);
            contact.setPhone(data);
        } else if (flagEmail) {
            Root.Contact contact = contacts.get(size);
            contact.setEmail(data);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (localName.equalsIgnoreCase("contact")) {
            size++;
        }
        resetDataTag();
    }

    //chua co namespace localName, qName cung 1 value
    @Override
    public void startElement(String uri, String localName, String qName, Attributes atrbts) throws SAXException {
        resetDataTag();

        //get and write attr in tag contact
        if (localName.equalsIgnoreCase("contact")) {
            Root.Contact contact = new Root.Contact();

            String id = atrbts.getValue("id");
            contact.setId(id);

            contacts.add(size, contact);
        } //go to tag first_name
        else if (localName.equalsIgnoreCase("first_name")) {
            flagFirstName = true;
        } //go to tag last_name
        else if (localName.equalsIgnoreCase("last_name")) {
            flagLastName = true;
        } //go to tag company_name
        else if (localName.equalsIgnoreCase("company_name")) {
            flagCompanyName = true;
        } //go to tag address
        else if (localName.equalsIgnoreCase("address")) {
            flagAddress = true;
        } //go to tag city
        else if (localName.equalsIgnoreCase("city")) {
            flagCity = true;
        } //go to tag county
        else if (localName.equalsIgnoreCase("county")) {
            flagCounty = true;
        } //go to tag state
        else if (localName.equalsIgnoreCase("state")) {
            flagState = true;
        } //go to tag zip
        else if (localName.equalsIgnoreCase("zip")) {
            flagZip = true;
        } //go to tag phone1
        else if (localName.equalsIgnoreCase("phone1")) {
            flagPhone1 = true;
        } //go to tag phone
        else if (localName.equalsIgnoreCase("phone")) {
            flagPhone = true;
        } //go to tag email
        else if (localName.equalsIgnoreCase("email")) {
            flagEmail = true;
        }

    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void startDocument() throws SAXException {
        //System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    }

}
