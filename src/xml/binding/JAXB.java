/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package xml.binding;

import generated.Root;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Admin
 */
public class JAXB {

    //Read file
    public List<Root.Contact> readFile(String filePath) {
        try {
            JAXBContext jAXBContext = JAXBContext.newInstance(Root.class);
            Unmarshaller unmarshaller = jAXBContext.createUnmarshaller();
            Root root = (Root) unmarshaller.unmarshal(new File(filePath));
            return root.getContact();
        } catch (JAXBException ex) {
            Logger.getLogger(JAXB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //Write file
    public Boolean writeFile(Root root, String filePath) {
        try {
            JAXBContext jAXBContext = JAXBContext.newInstance(Root.class);
            Marshaller marshaller = jAXBContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(root, new File(filePath));
            return true;
        } catch (JAXBException ex) {
            Logger.getLogger(JAXB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
