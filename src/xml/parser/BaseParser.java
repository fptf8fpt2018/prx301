/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package xml.parser;

import java.util.List;

/**
 *
 * @author Admin
 * @param <T>
 */
public interface BaseParser<T> {

    /**
     * Read file XML at location "filePath" parser to collection of generic
     * class corresponding.
     *
     * @param filePath
     * @return
     */
    List<T> readFile(String filePath);

    /**
     * Write collection of generic class to file XML corresponding at location
     * "filePath".
     *
     * @param filePath
     * @param source
     * @return
     */
    Boolean writeFile(String filePath, List<T> source);
    
    /**
     * Append collection of generic class to file XML corresponding at location
     * "filePath".
     *
     * @param filePath
     * @param appendSource
     * @return
     */
    Boolean appendFile(String filePath, List<T> appendSource);
}
