/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package xml.parser;

import generated.Root;
import java.io.File;
import java.util.List;
import xml.handler.XMLNameTag;

/**
 *
 * @author Admin
 */
public class DOMParser<T> implements BaseParser<T> {

    //Classify type of generic class;
    XMLNameTag xmlTag;

    public DOMParser(XMLNameTag xmlTag) {
        this.xmlTag = xmlTag;
    }

    @Override
    public List<T> readFile(String filePath) {
        //XML Contact to collection of Contact
        if (xmlTag.equals(XMLNameTag.Contact)) {
            xml.handler.contact.DOM contactHandler
                    = new xml.handler.contact.DOM();
            contactHandler.readFile(filePath);
            return (List<T>) contactHandler.contacts;
        } //XML ...tag to collection of ...class
        else if (xmlTag.equals(XMLNameTag.AnythingElse)) {
            return null;
        }
        return null;
    }

    @Override
    public Boolean writeFile(String filePath, List<T> source) {
        //collection of Contact to XML Contact 
        if (xmlTag.equals(XMLNameTag.Contact)) {
            xml.handler.contact.DOM contactHandler
                    = new xml.handler.contact.DOM();
            return contactHandler
                    .writeFile(filePath, (List<Root.Contact>) source);
        } //collection of ...class to XML ...tag
        else if (xmlTag.equals(XMLNameTag.AnythingElse)) {
            return false;
        }
        return false;
    }

    @Override
    public Boolean appendFile(String filePath, List<T> appendSource) {
        //collection of Contact to XML Contact 
        if (xmlTag.equals(XMLNameTag.Contact)) {
            xml.handler.contact.DOM contactHandler
                    = new xml.handler.contact.DOM();
            return contactHandler
                    .appendFile(filePath, (List<Root.Contact>) appendSource);
        } //collection of ...class to XML ...tag
        else if (xmlTag.equals(XMLNameTag.AnythingElse)) {
            return false;
        }
        return false;
    }

}
