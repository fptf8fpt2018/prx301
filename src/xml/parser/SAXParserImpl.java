/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package xml.parser;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import xml.handler.XMLNameTag;

/**
 *
 * @author Admin
 * @param <T>
 */
public class SAXParserImpl<T> implements BaseParser<T> {

    //Classify type of generic class;
    XMLNameTag xmlTag;

    public SAXParserImpl(XMLNameTag xmlTag) {
        this.xmlTag = xmlTag;
    }

    @Override
    public List<T> readFile(String filePath) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true); //no them chuc nang xu ly namespace
            SAXParser parser = factory.newSAXParser();

            //XML Contact to collection of Contact
            if (xmlTag.equals(XMLNameTag.Contact)) {
                xml.handler.contact.SAX contactHandler 
                        = new xml.handler.contact.SAX();
                parser.parse(new File(filePath), contactHandler);
                return (List<T>) contactHandler.contacts;
            } //XML ...tag to collection of ...class
            else if (xmlTag.equals(XMLNameTag.AnythingElse)) {
                return null;
            }

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(SAXParserImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Boolean writeFile(String filePath, List<T> source) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean appendFile(String filePath, List<T> appendSource) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
