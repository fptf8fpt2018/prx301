
import generated.Root;
import java.util.ArrayList;
import java.util.List;
import xml.handler.XMLNameTag;
import xml.parser.DOMParser;
import xml.parser.JDOMParser;
import xml.parser.SAXParserImpl;
import xml.parser.StAXParser;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Admin
 */
public class MainTestData {

    public static void main(String[] args) {
        //SAX(read)
        List<Root.Contact> sourceSAX = testSAXReadFile();
        System.out.println("------------------------------------");

        //StAX(read, write)
        //read
        List<Root.Contact> sourceStAX = testStAXReadFile();
        //write
        testStAXWriteFile("StAX_write_file.xml", sourceStAX);
        System.out.println("------------------------------------");

        //JDOM(read, write)
        //read
        List<Root.Contact> sourceJDOM = testJDOMReadFile();
        //write
        testJDOMWriteFile("JDOM_write_file.xml", sourceJDOM);
        System.out.println("------------------------------------");

        //DOM(read, write, append)
        //read
        List<Root.Contact> sourceDOM = testDOMReadFile();
        //write
        testDOMWriteFile("DOM_write_file.xml", sourceDOM);
        System.out.println("------------------------------------");
        //append(file must existed before)
        String fileName = "DOM_append_file.xml";
        testDOMWriteFile(fileName, sourceDOM);//create file before
        List<Root.Contact> sourceSAXmixStAX = new ArrayList<>();
        sourceSAXmixStAX.addAll(sourceSAX);
        sourceSAXmixStAX.addAll(sourceStAX);
        testDOMAppendFile(fileName, sourceSAXmixStAX);

    }

    static List<Root.Contact> testSAXReadFile() {
        SAXParserImpl<Root.Contact> parser = new SAXParserImpl<>(XMLNameTag.Contact);
        List<Root.Contact> contacts = parser.readFile("contacts.xml");
        System.out.println("SAX read: " + contacts.size());
        return contacts;
    }

    static List<Root.Contact> testDOMReadFile() {
        DOMParser<Root.Contact> parser = new DOMParser<>(XMLNameTag.Contact);
        List<Root.Contact> contacts = parser.readFile("contacts.xml");
        System.out.println("DOM read: " + contacts.size());
        return contacts;
    }

    static void testDOMWriteFile(String filePath, List<Root.Contact> source) {
        DOMParser<Root.Contact> parser = new DOMParser<>(XMLNameTag.Contact);
        Boolean result = parser.writeFile(filePath, source);
        System.out.println("DOM write: "
                + filePath
                + " "
                + (result ? "successful" : "failed"));
    }

    static void testDOMAppendFile(String filePath, List<Root.Contact> source) {
        DOMParser<Root.Contact> parser = new DOMParser<>(XMLNameTag.Contact);
        Boolean result = parser.appendFile(filePath, source);
        System.out.println("DOM append: "
                + filePath
                + " "
                + (result ? "successful" : "failed"));
    }

    private static List<Root.Contact> testStAXReadFile() {
        StAXParser<Root.Contact> parser = new StAXParser<>(XMLNameTag.Contact);
        List<Root.Contact> contacts = parser.readFile("contacts.xml");
        System.out.println("StAX read: " + contacts.size());
        return contacts;
    }

    static void testStAXWriteFile(String filePath, List<Root.Contact> source) {
        StAXParser<Root.Contact> parser = new StAXParser<>(XMLNameTag.Contact);
        Boolean result = parser.writeFile(filePath, source);
        System.out.println("StAX write: "
                + filePath
                + " "
                + (result ? "successful" : "failed"));
    }

    private static List<Root.Contact> testJDOMReadFile() {
        JDOMParser<Root.Contact> parser = new JDOMParser<>(XMLNameTag.Contact);
        List<Root.Contact> contacts = parser.readFile("contacts.xml");
        System.out.println("JDOM read: " + contacts.size());
        return contacts;
    }

    private static void testJDOMWriteFile(String filePath, List<Root.Contact> source) {
        JDOMParser<Root.Contact> parser = new JDOMParser<>(XMLNameTag.Contact);
        Boolean result = parser.writeFile(filePath, source);
        System.out.println("JDOM write: "
                + filePath
                + " "
                + (result ? "successful" : "failed"));
    }

}
