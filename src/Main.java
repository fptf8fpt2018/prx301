/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMain.java to edit this template
 */

import generated.Root;
import generated.Root.Contact;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import xml.handler.XMLNameTag;
import xml.parser.DOMParser;
import xml.parser.SAXParserImpl;
import xml.parser.StAXParser;

/**
 *
 * @author Admin
 */
//priority form add new 
//sua lai file delete ko phai la file temp
//turn on again full-screen mode
//bat su kien khong chon
//validate data input/update
//lam lai cai border header
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        //Data Logic
        List<Root.Contact> contacts = getContacts();
        //Data GUI
        TableView<Root.Contact> tblViewContacts = getTblViewContacts(contacts);
        //Data need to filter source(data logic) to get clean data(new data GUI) to display 
        ComboBox<String> cboSearchBy = getCboSearchBy();
        TextField txtSearchInfo = getTxtSearchInfo();

        Group root = new Group();
        ObservableList observableList = root.getChildren();
        observableList.add(getLblSearchBy());
        observableList.add(cboSearchBy);
        observableList.add(getLblSearchInfo());
        observableList.add(txtSearchInfo);
        observableList.add(getBtnSearch(cboSearchBy, txtSearchInfo, tblViewContacts, contacts));
        observableList.add(tblViewContacts);
        observableList.add(getBtnAddNew(tblViewContacts, contacts));
        observableList.add(getBtnUpdate(tblViewContacts, contacts));
        observableList.add(getBtnDelete(tblViewContacts, contacts));

        Scene scene = new Scene(root, 1000, 700);
        //Append child component in primary stage.
        primaryStage.setScene(scene);
        handlePrimaryStageCode(primaryStage);

//        //ref https://stackoverflow.com/questions/43747405/javafx-aligning-all-content-in-a-flowpane
//        FlowPane flowPane = new FlowPane();
//        flowPane.getChildren().add(lblSearchBy);
//        flowPane.getChildren().add(cboSearchBy);
//        flowPane.getChildren().add(lblSearchInfo);
//        flowPane.getChildren().add(txtSearchInfo);
//        flowPane.getChildren().add(btnSearch);
//
//        flowPane.setBorder(new Border(new BorderStroke(Color.BLACK,
//                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
//        flowPane.setStyle("-fx-background-color:#EEEEEE");
//        flowPane.setMinWidth(1800);
//        flowPane.setMaxWidth(1800);
////        flowPane.maxHeight(500);
//        flowPane.setPadding(new Insets(30, 30, 30, 30));
//
//        observableList.add(flowPane);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    //Get name of all element and attribute in XML document.
    private List<String> loadCbxByJAXB() {
        List<Field> fields = getFields(new Root.Contact());
        List<String> fieldsName = new ArrayList<>();

        //get name in annotation(in xml(element, attribute)) if exists prefer than class properties name(in java) 
        //ref https://stackoverflow.com/questions/60035612/getting-annotation-values-from-xmlelement-in-a-java-class
        fields.stream().map((Field field) -> {
            Annotation[] annotationsXMLEle = field.getAnnotationsByType(XmlElement.class);
            //Traverse xml element annotation array
            for (int i = 0; i < annotationsXMLEle.length; i++) {
                Annotation annotation = annotationsXMLEle[i];
                //annotation is XmlElement.
                if (annotation instanceof XmlElement) {
                    XmlElement theElement = (XmlElement) annotation;
                    //more than one word (get name by xml tag name)
                    if (!theElement.name().equals("##default")) {
                        fieldsName.add(theElement.name());
                    } //one single word (get name by java class field name)
                    else {
                        fieldsName.add(field.getName());
                    }
                }
            }
            return field;
        }).forEachOrdered(field -> {
            Annotation[] annotationsXMLAttr = field.getAnnotationsByType(XmlAttribute.class);
            //Traverse xml attribute annotation array 
            for (int i = 0; i < annotationsXMLAttr.length; i++) {
                Annotation annotation = annotationsXMLAttr[i];
                //annotation is XmlAttribute.
                if (annotation instanceof XmlAttribute) {
                    XmlAttribute theAttr = (XmlAttribute) annotation;
                    //more than one word (get attr by xml attr name)
                    if (!theAttr.name().equals("##default")) {
                        fieldsName.add(theAttr.name());
                    }//one single word (get attr by java class field name)
                    else {
                        fieldsName.add(field.getName());
                    }
                }
            }
        });
        return fieldsName;
    }

    //Get all fields of a class.
    //ref https://stackoverflow.com/questions/16295949/get-all-fields-even-private-and-inherited-from-class
    private <T> List<Field> getFields(T t) {
        List<Field> fields = new ArrayList<>();
        Class clazz = t.getClass();
        //duyet tu class con len cha de lay het fields
        while (clazz != Object.class) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

    //Code using primary stage component handle here.
    private void handlePrimaryStageCode(Stage primaryStage) {
        primaryStage.setTitle("Contract Management");
        //Set fixed size of stage, scene.
        primaryStage.setResizable(false);
        //Start position of window in center of minitor.
        primaryStage.centerOnScreen();
        //Begin full-screen mode at running time.
        primaryStage.setFullScreen(true);
        //High, width cover all screen(without taskbar if exist)
        setFullscreenMode(primaryStage);

        primaryStage.show();
        //Remove three window buttons.
        //primaryStage.initStyle(StageStyle.UNDECORATED);
    }

    //Same with primaryStage.setMaximized(true); 
    //but it can'tcombine with full-screen mode of system's code.
    private void setFullscreenMode(Stage primaryStage) {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
    }

    //
    private List<Root.Contact> getContacts() {
        SAXParserImpl<Root.Contact> parser = new SAXParserImpl<>(XMLNameTag.Contact);
        List<Root.Contact> contacts = parser.readFile("contacts.xml");
        return contacts;
    }

    private Label getLblSearchBy() {
        Label lblSearchBy = new Label();
        lblSearchBy.setText("Search by: ");
        lblSearchBy.setLayoutX(100);
        lblSearchBy.setLayoutY(50);
        return lblSearchBy;
    }

    private ComboBox<String> getCboSearchBy() {
        ComboBox<String> cboSearchBy
                = new ComboBox<>(FXCollections.observableArrayList(loadCbxByJAXB()));
        cboSearchBy.setLayoutX(200);
        cboSearchBy.setLayoutY(50);
        cboSearchBy.getSelectionModel().selectFirst();
        return cboSearchBy;
    }

    private Label getLblSearchInfo() {
        Label lblSearchInfo = new Label();
        lblSearchInfo.setText("Search info: ");
        lblSearchInfo.setLayoutX(400);
        lblSearchInfo.setLayoutY(50);
        return lblSearchInfo;
    }

    private TextField getTxtSearchInfo() {
        TextField txtSearchInfo = new TextField();
        txtSearchInfo.setLayoutX(500);
        txtSearchInfo.setLayoutY(50);
        txtSearchInfo.setPrefSize(1100, txtSearchInfo.getHeight());
        return txtSearchInfo;
    }

    private Button getBtnSearch(ComboBox<String> cboSearchBy,
            TextField txtSearchInfo, TableView<Contact> tblViewContacts,
            List<Contact> contacts) {
        Button btnSearch = new Button();
        btnSearch.setText("Search");
        btnSearch.setLayoutX(1700);
        btnSearch.setLayoutY(50);
        btnSearch.setPrefSize(100, btnSearch.getHeight());

        btnSearch.setOnAction((ActionEvent event) -> {
            String searchBy = cboSearchBy.getValue();
            String searchInfo = txtSearchInfo.getText();
            List<String> allSearchBy = loadCbxByJAXB();

            List<Contact> filteredData = new ArrayList<>();
            //valid argument.
            if (allSearchBy.contains(searchBy) && !searchInfo.equals("")) {
                switch (searchBy) {
                    case "first_name": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getFirstName())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "last_name": {
                        filteredData = contacts
                                .stream()
                                .filter(new Predicate<Contact>() {
                                    @Override
                                    public boolean test(Contact contact) {
                                        return Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getLastName())
                                                .find();
                                    }
                                })
                                .collect(Collectors.toList());
                        break;
                    }
                    case "company_name": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getCompanyName())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "address": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getAddress())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "city": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getCity())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "county": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getCounty())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "state": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getState())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "zip": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getZip().toString())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "phone1": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getPhone1())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "phone": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getPhone())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "email": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getEmail())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                    case "id": {
                        filteredData = contacts
                                .stream()
                                .filter(
                                        contact
                                        -> Pattern
                                                .compile(
                                                        Pattern.quote(searchInfo),
                                                        Pattern.CASE_INSENSITIVE)
                                                .matcher(contact.getId())
                                                .find()
                                )
                                .collect(Collectors.toList());
                        break;
                    }
                }

                //xoa het du lieu cu(day du)
                tblViewContacts.getItems().removeAll(contacts);
                //hien thi du lieu moi(1 phan)
                tblViewContacts.getItems().addAll(filteredData);
            } //keyword is blank
            else {
                //xoa het du lieu cu(1 phan), tat ca chua 1 phan(tranh bi lac mat con tro)
                tblViewContacts.getItems().removeAll(contacts);
                //hien thi du lieu moi(day du)
                tblViewContacts.getItems().addAll(contacts);
            }

        });
        return btnSearch;
    }

    private TableView<Root.Contact> getTblViewContacts(List<Root.Contact> contacts) {
        TableView<Root.Contact> tableView = new TableView<>();
        tableView.setLayoutX(50);
        tableView.setLayoutY(150);
        tableView.setMinWidth(1800);
        tableView.setMaxWidth(1800);
        tableView.setMinHeight(700);
        tableView.setMaxHeight(700);

        //ref: https://genotechies.medium.com/javafx-tableview-with-dynamically-filled-data-from-a-list-89ff6f8778e1
        TableColumn<Root.Contact, String> cl1 = new TableColumn<>("ID");
        cl1.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn<Root.Contact, String> cl2 = new TableColumn<>("First Name");
        cl2.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        TableColumn<Root.Contact, String> cl3 = new TableColumn<>("Last Name");
        cl3.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        TableColumn<Root.Contact, String> cl4 = new TableColumn<>("Company Name");
        cl4.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        TableColumn<Root.Contact, String> cl5 = new TableColumn<>("Address");
        cl5.setCellValueFactory(new PropertyValueFactory<>("address"));
        TableColumn<Root.Contact, String> cl6 = new TableColumn<>("City");
        cl6.setCellValueFactory(new PropertyValueFactory<>("city"));
        TableColumn<Root.Contact, String> cl7 = new TableColumn<>("County");
        cl7.setCellValueFactory(new PropertyValueFactory<>("county"));
        TableColumn<Root.Contact, String> cl8 = new TableColumn<>("State");
        cl8.setCellValueFactory(new PropertyValueFactory<>("state"));
        TableColumn<Root.Contact, BigInteger> cl9 = new TableColumn<>("Zip");
        cl9.setCellValueFactory(new PropertyValueFactory<>("zip"));
        TableColumn<Root.Contact, String> cl10 = new TableColumn<>("Phone");
        cl10.setCellValueFactory(new PropertyValueFactory<>("phone"));
        TableColumn<Root.Contact, String> cl11 = new TableColumn<>("Email");
        cl11.setCellValueFactory(new PropertyValueFactory<>("email"));

        // Add two columns into TableView
        tableView.getColumns().add(cl1);
        tableView.getColumns().add(cl2);
        tableView.getColumns().add(cl3);
        tableView.getColumns().add(cl4);
        tableView.getColumns().add(cl5);
        tableView.getColumns().add(cl6);
        tableView.getColumns().add(cl7);
        tableView.getColumns().add(cl8);
        tableView.getColumns().add(cl9);
        tableView.getColumns().add(cl10);
        tableView.getColumns().add(cl11);
        //Remove last blank column.
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        tableView.getItems().addAll(contacts);
        return tableView;
    }

    private Button getBtnAddNew(TableView<Root.Contact> tblViewContacts, List<Root.Contact> contacts) {
        Button btnAddNew = new Button();
        btnAddNew.setText("Add new");
        btnAddNew.setLayoutX(50);
        btnAddNew.setLayoutY(900);
        btnAddNew.setPrefSize(100, btnAddNew.getHeight());

        btnAddNew.setOnAction((ActionEvent event) -> {
            Group rootAddNew = new Group();
            ObservableList observableListAddNew = rootAddNew.getChildren();
            Stage stageAddNew = new Stage();
            stageAddNew.setTitle("");
            //Set fixed size of stage, scene.
            stageAddNew.setResizable(false);
            //Start position of window in center of minitor.
            stageAddNew.centerOnScreen();
            //Size(height, width)
            stageAddNew.setHeight(900);
            stageAddNew.setWidth(700);
            //best prioirty, ref https://stackoverflow.com/questions/12819194/javafx-2-2-stage-always-on-top
            //stageAddNew.setAlwaysOnTop(true);
            stageAddNew.initModality(Modality.APPLICATION_MODAL);//effect like system mat 2 nut phong to, thu nho

            Label lblTitle = new Label();
            lblTitle.setText("Contact info");
            lblTitle.setLayoutX(200);
            lblTitle.setLayoutY(0);
            lblTitle.setFont(new Font(50.0));
            lblTitle.setStyle("-fx-font-weight: bold");
            //lblTitle.setPrefSize(0, 300);
            observableListAddNew.add(lblTitle);

            //Row 1
            Label lblID = new Label();
            lblID.setText("ID");
            lblID.setLayoutX(50);
            lblID.setLayoutY(100);
            lblID.setFont(new Font(20.0));
            lblID.setPrefSize(100, lblID.getHeight());
            observableListAddNew.add(lblID);
            TextField txtID = new TextField();
            txtID.setText("");
            txtID.setLayoutX(150);
            txtID.setLayoutY(100);
            txtID.setPrefSize(500, txtID.getHeight());
            txtID.setDisable(false);
            observableListAddNew.add(txtID);

            //Row 2
            Label lblFirstname = new Label();
            lblFirstname.setText("First name");
            lblFirstname.setLayoutX(50);
            lblFirstname.setLayoutY(150);
            lblFirstname.setFont(new Font(20.0));
            lblFirstname.setPrefSize(100, lblFirstname.getHeight());
            observableListAddNew.add(lblFirstname);
            TextField txtFirstName = new TextField();
            txtFirstName.setText("");
            txtFirstName.setLayoutX(150);
            txtFirstName.setLayoutY(150);
            txtFirstName.setPrefSize(500, txtFirstName.getHeight());
            observableListAddNew.add(txtFirstName);

            //Row 3
            Label lblLastname = new Label();
            lblLastname.setText("Last name");
            lblLastname.setLayoutX(50);
            lblLastname.setLayoutY(200);
            lblLastname.setFont(new Font(20.0));
            lblLastname.setPrefSize(100, lblLastname.getHeight());
            observableListAddNew.add(lblLastname);
            TextField txtLastName = new TextField();
            txtLastName.setText("");
            txtLastName.setLayoutX(150);
            txtLastName.setLayoutY(200);
            txtLastName.setPrefSize(500, txtLastName.getHeight());
            observableListAddNew.add(txtLastName);

            //Row 4
            Label lblCompany = new Label();
            lblCompany.setText("Company");
            lblCompany.setLayoutX(50);
            lblCompany.setLayoutY(250);
            lblCompany.setFont(new Font(20.0));
            lblCompany.setPrefSize(100, lblCompany.getHeight());
            observableListAddNew.add(lblCompany);
            TextField txtCompany = new TextField();
            txtCompany.setText("");
            txtCompany.setLayoutX(150);
            txtCompany.setLayoutY(250);
            txtCompany.setPrefSize(500, txtCompany.getHeight());
            observableListAddNew.add(txtCompany);

            //Row 5
            Label lblAddress = new Label();
            lblAddress.setText("Address");
            lblAddress.setLayoutX(50);
            lblAddress.setLayoutY(300);
            lblAddress.setFont(new Font(20.0));
            lblAddress.setPrefSize(100, lblAddress.getHeight());
            observableListAddNew.add(lblAddress);
            TextField txtAddress = new TextField();
            txtAddress.setText("");
            txtAddress.setLayoutX(150);
            txtAddress.setLayoutY(300);
            txtAddress.setPrefSize(500, txtAddress.getHeight());
            observableListAddNew.add(txtAddress);

            //Row 6
            Label lblCity = new Label();
            lblCity.setText("City");
            lblCity.setLayoutX(50);
            lblCity.setLayoutY(350);
            lblCity.setFont(new Font(20.0));
            lblCity.setPrefSize(100, lblCity.getHeight());
            observableListAddNew.add(lblCity);
            TextField txtCity = new TextField();
            txtCity.setText("");
            txtCity.setLayoutX(150);
            txtCity.setLayoutY(350);
            txtCity.setPrefSize(500, txtCity.getHeight());
            observableListAddNew.add(txtCity);

            //Row 7
            Label lblCountry = new Label();
            lblCountry.setText("Country");
            lblCountry.setLayoutX(50);
            lblCountry.setLayoutY(400);
            lblCountry.setFont(new Font(20.0));
            lblCountry.setPrefSize(100, lblCountry.getHeight());
            observableListAddNew.add(lblCountry);
            TextField txtCountry = new TextField();
            txtCountry.setText("");
            txtCountry.setLayoutX(150);
            txtCountry.setLayoutY(400);
            txtCountry.setPrefSize(500, txtCountry.getHeight());
            observableListAddNew.add(txtCountry);

            //Row 8
            Label lblState = new Label();
            lblState.setText("State");
            lblState.setLayoutX(50);
            lblState.setLayoutY(450);
            lblState.setFont(new Font(20.0));
            lblState.setPrefSize(100, lblState.getHeight());
            observableListAddNew.add(lblState);
            TextField txtState = new TextField();
            txtState.setText("");
            txtState.setLayoutX(150);
            txtState.setLayoutY(450);
            txtState.setPrefSize(500, txtState.getHeight());
            observableListAddNew.add(txtState);

            //Row 9
            Label lblZip = new Label();
            lblZip.setText("Zip");
            lblZip.setLayoutX(50);
            lblZip.setLayoutY(500);
            lblZip.setFont(new Font(20.0));
            lblZip.setPrefSize(100, lblZip.getHeight());
            observableListAddNew.add(lblZip);
            TextField txtZip = new TextField();
            txtZip.setText("");
            txtZip.setLayoutX(150);
            txtZip.setLayoutY(500);
            txtZip.setPrefSize(500, txtZip.getHeight());
            observableListAddNew.add(txtZip);

            //Row 10
            Label lblPhone1 = new Label();
            lblPhone1.setText("Phone 1");
            lblPhone1.setLayoutX(50);
            lblPhone1.setLayoutY(550);
            lblPhone1.setFont(new Font(20.0));
            lblPhone1.setPrefSize(100, lblPhone1.getHeight());
            observableListAddNew.add(lblPhone1);
            TextField txtPhone1 = new TextField();
            txtPhone1.setText("");
            txtPhone1.setLayoutX(150);
            txtPhone1.setLayoutY(550);
            txtPhone1.setPrefSize(500, txtPhone1.getHeight());
            observableListAddNew.add(txtPhone1);

            //Row 11
            Label lblPhone = new Label();
            lblPhone.setText("Phone");
            lblPhone.setLayoutX(50);
            lblPhone.setLayoutY(600);
            lblPhone.setFont(new Font(20.0));
            lblPhone.setPrefSize(100, lblPhone.getHeight());
            observableListAddNew.add(lblPhone);
            TextField txtPhone = new TextField();
            txtPhone.setText("");
            txtPhone.setLayoutX(150);
            txtPhone.setLayoutY(600);
            txtPhone.setPrefSize(500, txtPhone.getHeight());
            observableListAddNew.add(txtPhone);

            //Row 12
            Label lblEmail = new Label();
            lblEmail.setText("Email");
            lblEmail.setLayoutX(50);
            lblEmail.setLayoutY(650);
            lblEmail.setFont(new Font(20.0));
            lblEmail.setPrefSize(100, lblEmail.getHeight());
            observableListAddNew.add(lblEmail);
            TextField txtEmail = new TextField();
            txtEmail.setText("");
            txtEmail.setLayoutX(150);
            txtEmail.setLayoutY(650);
            txtEmail.setPrefSize(500, txtEmail.getHeight());
            observableListAddNew.add(txtEmail);

            Button btnAddNewSave = new Button();
            btnAddNewSave.setText("Save");
            btnAddNewSave.setLayoutX(150);
            btnAddNewSave.setLayoutY(750);
            btnAddNewSave.setFont(new Font(30.0));
            btnAddNewSave.setPrefSize(150, btnAddNewSave.getHeight());
            btnAddNewSave.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Root.Contact addedContact = new Root.Contact();
                    addedContact.setId(txtID.getText());
                    addedContact.setFirstName(txtFirstName.getText());
                    addedContact.setLastName(txtLastName.getText());
                    addedContact.setCompanyName(txtCompany.getText());
                    addedContact.setAddress(txtAddress.getText());
                    addedContact.setCity(txtCity.getText());
                    addedContact.setCounty(txtCountry.getText());
                    addedContact.setState(txtState.getText());
                    addedContact.setZip(new BigInteger(txtZip.getText()));
                    addedContact.setPhone1(txtPhone1.getText());
                    addedContact.setPhone(txtPhone.getText());
                    addedContact.setEmail(txtEmail.getText());

                    //Update data logic
                    contacts.add(addedContact);

                    //Update data GUI
                    tblViewContacts.getItems().add(addedContact);

                    //Update data source(xml) using parser
                    StAXParser<Root.Contact> parser = new StAXParser<>(XMLNameTag.Contact);
                    parser.writeFile("contacts.xml", contacts);

                    Alert alertInfo = new Alert(AlertType.INFORMATION);
                    alertInfo.setTitle("Notification");

                    // Header Text: null
                    alertInfo.setHeaderText("Added this contact successfully!");
                    alertInfo.setContentText(addedContact.toString());
                    alertInfo.showAndWait();
                    stageAddNew.close();
                }

            });
            observableListAddNew.add(btnAddNewSave);

            Button btnAddNewCancel = new Button();
            btnAddNewCancel.setText("Cancel");
            btnAddNewCancel.setLayoutX(400);
            btnAddNewCancel.setLayoutY(750);
            btnAddNewCancel.setFont(new Font(30.0));
            btnAddNewCancel.setPrefSize(150, btnAddNewSave.getHeight());
            btnAddNewCancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    stageAddNew.close();
                }

            });
            observableListAddNew.add(btnAddNewCancel);

            Scene sceneAddNew = new Scene(rootAddNew, 1000, 700);
            //Append child component in primary stage.
            stageAddNew.setScene(sceneAddNew);
            stageAddNew.show();
            // Hide this current window (if this is what you want)
            //((Node) (event.getSource())).getScene().getWindow().hide();
        });
        return btnAddNew;
    }

    private Button getBtnUpdate(TableView<Root.Contact> tblViewContacts, List<Root.Contact> contacts) {
        Button btnUpdate = new Button();
        btnUpdate.setText("Update");
        btnUpdate.setLayoutX(200);
        btnUpdate.setLayoutY(900);
        btnUpdate.setPrefSize(100, btnUpdate.getHeight());

        btnUpdate.setOnAction((ActionEvent event) -> {
            Group rootAddNew = new Group();
            ObservableList observableListAddNew = rootAddNew.getChildren();
            Stage stageAddNew = new Stage();
            stageAddNew.setTitle("");
            //Set fixed size of stage, scene.
            stageAddNew.setResizable(false);
            //Start position of window in center of minitor.
            stageAddNew.centerOnScreen();
            //Size(height, width)
            stageAddNew.setHeight(900);
            stageAddNew.setWidth(700);
            //best prioirty, ref https://stackoverflow.com/questions/12819194/javafx-2-2-stage-always-on-top
            //stageAddNew.setAlwaysOnTop(true);
            stageAddNew.initModality(Modality.APPLICATION_MODAL);//effect like system mat 2 nut phong to, thu nho

            Root.Contact updatedContact
                    = tblViewContacts.getSelectionModel().getSelectedItem();

            Label lblTitle = new Label();
            lblTitle.setText("Contact info");
            lblTitle.setLayoutX(200);
            lblTitle.setLayoutY(0);
            lblTitle.setFont(new Font(50.0));
            lblTitle.setStyle("-fx-font-weight: bold");
            //lblTitle.setPrefSize(0, 300);
            observableListAddNew.add(lblTitle);

            //Row 1
            Label lblID = new Label();
            lblID.setText("ID");
            lblID.setLayoutX(50);
            lblID.setLayoutY(100);
            lblID.setFont(new Font(20.0));
            lblID.setPrefSize(100, lblID.getHeight());
            observableListAddNew.add(lblID);
            TextField txtID = new TextField();
            txtID.setText(updatedContact.getId());
            txtID.setLayoutX(150);
            txtID.setLayoutY(100);
            txtID.setPrefSize(500, txtID.getHeight());
            txtID.setDisable(true);
            observableListAddNew.add(txtID);

            //Row 2
            Label lblFirstname = new Label();
            lblFirstname.setText("First name");
            lblFirstname.setLayoutX(50);
            lblFirstname.setLayoutY(150);
            lblFirstname.setFont(new Font(20.0));
            lblFirstname.setPrefSize(100, lblFirstname.getHeight());
            observableListAddNew.add(lblFirstname);
            TextField txtFirstName = new TextField();
            txtFirstName.setText(updatedContact.getFirstName());
            txtFirstName.setLayoutX(150);
            txtFirstName.setLayoutY(150);
            txtFirstName.setPrefSize(500, txtFirstName.getHeight());
            observableListAddNew.add(txtFirstName);

            //Row 3
            Label lblLastname = new Label();
            lblLastname.setText("Last name");
            lblLastname.setLayoutX(50);
            lblLastname.setLayoutY(200);
            lblLastname.setFont(new Font(20.0));
            lblLastname.setPrefSize(100, lblLastname.getHeight());
            observableListAddNew.add(lblLastname);
            TextField txtLastName = new TextField();
            txtLastName.setText(updatedContact.getLastName());
            txtLastName.setLayoutX(150);
            txtLastName.setLayoutY(200);
            txtLastName.setPrefSize(500, txtLastName.getHeight());
            observableListAddNew.add(txtLastName);

            //Row 4
            Label lblCompany = new Label();
            lblCompany.setText("Company");
            lblCompany.setLayoutX(50);
            lblCompany.setLayoutY(250);
            lblCompany.setFont(new Font(20.0));
            lblCompany.setPrefSize(100, lblCompany.getHeight());
            observableListAddNew.add(lblCompany);
            TextField txtCompany = new TextField();
            txtCompany.setText(updatedContact.getCompanyName());
            txtCompany.setLayoutX(150);
            txtCompany.setLayoutY(250);
            txtCompany.setPrefSize(500, txtCompany.getHeight());
            observableListAddNew.add(txtCompany);

            //Row 5
            Label lblAddress = new Label();
            lblAddress.setText("Address");
            lblAddress.setLayoutX(50);
            lblAddress.setLayoutY(300);
            lblAddress.setFont(new Font(20.0));
            lblAddress.setPrefSize(100, lblAddress.getHeight());
            observableListAddNew.add(lblAddress);
            TextField txtAddress = new TextField();
            txtAddress.setText(updatedContact.getAddress());
            txtAddress.setLayoutX(150);
            txtAddress.setLayoutY(300);
            txtAddress.setPrefSize(500, txtAddress.getHeight());
            observableListAddNew.add(txtAddress);

            //Row 6
            Label lblCity = new Label();
            lblCity.setText("City");
            lblCity.setLayoutX(50);
            lblCity.setLayoutY(350);
            lblCity.setFont(new Font(20.0));
            lblCity.setPrefSize(100, lblCity.getHeight());
            observableListAddNew.add(lblCity);
            TextField txtCity = new TextField();
            txtCity.setText(updatedContact.getCity());
            txtCity.setLayoutX(150);
            txtCity.setLayoutY(350);
            txtCity.setPrefSize(500, txtCity.getHeight());
            observableListAddNew.add(txtCity);

            //Row 7
            Label lblCountry = new Label();
            lblCountry.setText("Country");
            lblCountry.setLayoutX(50);
            lblCountry.setLayoutY(400);
            lblCountry.setFont(new Font(20.0));
            lblCountry.setPrefSize(100, lblCountry.getHeight());
            observableListAddNew.add(lblCountry);
            TextField txtCountry = new TextField();
            txtCountry.setText(updatedContact.getCounty());
            txtCountry.setLayoutX(150);
            txtCountry.setLayoutY(400);
            txtCountry.setPrefSize(500, txtCountry.getHeight());
            observableListAddNew.add(txtCountry);

            //Row 8
            Label lblState = new Label();
            lblState.setText("State");
            lblState.setLayoutX(50);
            lblState.setLayoutY(450);
            lblState.setFont(new Font(20.0));
            lblState.setPrefSize(100, lblState.getHeight());
            observableListAddNew.add(lblState);
            TextField txtState = new TextField();
            txtState.setText(updatedContact.getState());
            txtState.setLayoutX(150);
            txtState.setLayoutY(450);
            txtState.setPrefSize(500, txtState.getHeight());
            observableListAddNew.add(txtState);

            //Row 9
            Label lblZip = new Label();
            lblZip.setText("Zip");
            lblZip.setLayoutX(50);
            lblZip.setLayoutY(500);
            lblZip.setFont(new Font(20.0));
            lblZip.setPrefSize(100, lblZip.getHeight());
            observableListAddNew.add(lblZip);
            TextField txtZip = new TextField();
            txtZip.setText(updatedContact.getZip().toString());
            txtZip.setLayoutX(150);
            txtZip.setLayoutY(500);
            txtZip.setPrefSize(500, txtZip.getHeight());
            observableListAddNew.add(txtZip);

            //Row 10
            Label lblPhone1 = new Label();
            lblPhone1.setText("Phone 1");
            lblPhone1.setLayoutX(50);
            lblPhone1.setLayoutY(550);
            lblPhone1.setFont(new Font(20.0));
            lblPhone1.setPrefSize(100, lblPhone1.getHeight());
            observableListAddNew.add(lblPhone1);
            TextField txtPhone1 = new TextField();
            txtPhone1.setText(updatedContact.getPhone1());
            txtPhone1.setLayoutX(150);
            txtPhone1.setLayoutY(550);
            txtPhone1.setPrefSize(500, txtPhone1.getHeight());
            observableListAddNew.add(txtPhone1);

            //Row 11
            Label lblPhone = new Label();
            lblPhone.setText("Phone");
            lblPhone.setLayoutX(50);
            lblPhone.setLayoutY(600);
            lblPhone.setFont(new Font(20.0));
            lblPhone.setPrefSize(100, lblPhone.getHeight());
            observableListAddNew.add(lblPhone);
            TextField txtPhone = new TextField();
            txtPhone.setText(updatedContact.getPhone());
            txtPhone.setLayoutX(150);
            txtPhone.setLayoutY(600);
            txtPhone.setPrefSize(500, txtPhone.getHeight());
            observableListAddNew.add(txtPhone);

            //Row 12
            Label lblEmail = new Label();
            lblEmail.setText("Email");
            lblEmail.setLayoutX(50);
            lblEmail.setLayoutY(650);
            lblEmail.setFont(new Font(20.0));
            lblEmail.setPrefSize(100, lblEmail.getHeight());
            observableListAddNew.add(lblEmail);
            TextField txtEmail = new TextField();
            txtEmail.setText(updatedContact.getEmail());
            txtEmail.setLayoutX(150);
            txtEmail.setLayoutY(650);
            txtEmail.setPrefSize(500, txtEmail.getHeight());
            observableListAddNew.add(txtEmail);

            Button btnAddNewSave = new Button();
            btnAddNewSave.setText("Save");
            btnAddNewSave.setLayoutX(150);
            btnAddNewSave.setLayoutY(750);
            btnAddNewSave.setFont(new Font(30.0));
            btnAddNewSave.setPrefSize(150, btnAddNewSave.getHeight());
            btnAddNewSave.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    //Update data logic
                    //Khong set duoc ID: addedContact.setId(btnAddNewSave.getText());
                    updatedContact.setFirstName(txtFirstName.getText());
                    updatedContact.setLastName(txtLastName.getText());
                    updatedContact.setCompanyName(txtCompany.getText());
                    updatedContact.setAddress(txtAddress.getText());
                    updatedContact.setCity(txtCity.getText());
                    updatedContact.setCounty(txtCountry.getText());
                    updatedContact.setState(txtState.getText());
                    updatedContact.setZip(new BigInteger(txtZip.getText()));
                    updatedContact.setPhone1(txtPhone1.getText());
                    updatedContact.setPhone(txtPhone.getText());
                    updatedContact.setEmail(txtEmail.getText());

                    //Update data GUI
                    tblViewContacts.refresh();

                    //Update data source(xml) using parser
                    StAXParser<Root.Contact> parser = new StAXParser<>(XMLNameTag.Contact);
                    parser.writeFile("contacts.xml", contacts);

                    Alert alertInfo = new Alert(AlertType.INFORMATION);
                    alertInfo.setTitle("Notification");

                    // Header Text: null
                    alertInfo.setHeaderText("Updated this contact successfully!");
                    alertInfo.setContentText(updatedContact.toString());
                    alertInfo.showAndWait();
                    stageAddNew.close();
                }

            });
            observableListAddNew.add(btnAddNewSave);

            Button btnAddNewCancel = new Button();
            btnAddNewCancel.setText("Cancel");
            btnAddNewCancel.setLayoutX(400);
            btnAddNewCancel.setLayoutY(750);
            btnAddNewCancel.setFont(new Font(30.0));
            btnAddNewCancel.setPrefSize(150, btnAddNewSave.getHeight());
            btnAddNewCancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    stageAddNew.close();
                }

            });
            observableListAddNew.add(btnAddNewCancel);

            Scene sceneAddNew = new Scene(rootAddNew, 1000, 700);
            //Append child component in primary stage.
            stageAddNew.setScene(sceneAddNew);
            stageAddNew.show();
            // Hide this current window (if this is what you want)
            //((Node) (event.getSource())).getScene().getWindow().hide();
        });
        return btnUpdate;
    }

    private Button getBtnDelete(TableView<Root.Contact> tblViewContacts, List<Root.Contact> contacts) {
        Button btnDelete = new Button();
        btnDelete.setText("Delete");
        btnDelete.setLayoutX(350);
        btnDelete.setLayoutY(900);
        btnDelete.setPrefSize(100, btnDelete.getHeight());
        btnDelete.setOnAction((ActionEvent event) -> {
            Root.Contact contact = tblViewContacts.getSelectionModel().getSelectedItem();

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Delete Contact");
            alert.setHeaderText("Are you sure want to delete this contact?");
            alert.setContentText(contact.toString());

            // option != null.
            Optional<ButtonType> option = alert.showAndWait();

            //user option
            if (option.get() == null) {
                //label.setText("No selection!");
            } else if (option.get() == ButtonType.OK) {
                //Delete data logic
                contacts.remove(contact);

                //Delete data GUI
                tblViewContacts.getItems().remove(contact);

                //Delete data source(xml) using parser
                DOMParser<Root.Contact> parser = new DOMParser<>(XMLNameTag.Contact);
                parser.writeFile("contacts.xml", contacts);

                Alert alertInfo = new Alert(AlertType.INFORMATION);
                alertInfo.setTitle("Notification");

                // Header Text: null
                alertInfo.setHeaderText(null);
                alertInfo.setContentText("Deleted this contact successfully!");
                alertInfo.showAndWait();

            } else if (option.get() == ButtonType.CANCEL) {
                //label.setText("Cancelled!");
            } else {
                //label.setText("-");
            }
        });

        return btnDelete;
    }

}
